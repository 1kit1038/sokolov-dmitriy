package ua.khpi.oop.sokolov13;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;

public class Threads {

    public static void start() throws InterruptedException {

        Random random = new Random();
        int time = random.nextInt(5);
        List<Planner> ls = new List<>();

        Runnable rn1 = ()->{
            try {
                Thread.sleep(2);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read1 = new Planner("12.06.2017 14:00", "ул. Академіка Павлова 271", 36, 0, "Марафон с++", "Студетны 118a, Студетны 118b");
                    read1.searchOfMinDuration("35:00");
                    System.out.println(read1);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Первй поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Runnable rn2 = ()->{
            try {
                Thread.sleep(3);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read2 = new Planner("12.04.2020 13:30", "ул.Жилянська 75", 72, 0, "Практика в EPAM", "Студетны 118a, Студетны 118b, Студетны 118є, Студетны 201a");
                    System.out.println(read2);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Второй поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Runnable rn3 = ()-> {
            try {
                Thread.sleep(2);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read3 = new Planner("15.06.2020 12:30; ул.Пушкинская 15/2; 12; 40; Собрание; Студетны 118е");
                    System.out.println(read3);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Третий поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Thread thr1 = new Thread(rn1);
        Thread thr2 = new Thread(rn2);
        Thread thr3 = new Thread(rn3);

        thr1.start();
        thr2.start();
        thr3.start();
        System.out.println("Все потоки запущенны");
        Thread.sleep(time);
        thr1.interrupt();
        thr2.interrupt();
        thr3.interrupt();



    }



}
