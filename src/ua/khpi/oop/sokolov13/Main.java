package ua.khpi.oop.sokolov13;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {


    public static void main(String @NotNull [] args) throws ParseException, InterruptedException {


        List<Planner> ls = new List<>();

        if (args.length == 1 && args[0].trim().equals("-auto")) {

            Threads.start();
            Thread.sleep(150);
            System.out.println("Все потоки остановленны");


        } else {


            Planner read1 = new Planner("12.06.2017 14:00", "ул. Академіка Павлова 271", 36, 0, "Марафон с++", "Студетны 118a, Студетны 118b");

            Planner read2 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 0, 55, "Консультация КСХ", "Студетны 118a, Студетны 201a");

            Planner read3 = new Planner("12.04.2019 13:30", "ул.Пушкинская 15/2", 0, 55, "Экзамен КСХ", "Студетны 118a, Студетны 201a");

            Planner read4 = new Planner("12.04.2018 13:30", "ул.Жилянська 75", 72, 0, "Практика в EPAM", "Студетны 118a, Студетны 118b, Студетны 118є, Студетны 201a");

            Pattern pat;
            Matcher mat;

            ls.addBack(read1);
            ls.addBack(read2);
            ls.addBack(read3);
            ls.removeBack();
            ls.removeFront();
            ls.addBack(read1);
            ls.addFront(read3);
            ls.addBack(read4);



            System.out.println("Выберите необходимое действие: \n" +
                    "   1 - Показать список событий\n" +
                    "   2 - Добавить\n" +
                    "   3 - Удалить\n" +
                    "   4 - Поиск\n" +
                    "   5 - Завершение работы\n");
            Scanner sc = new Scanner(System.in);

            String str;
            int s;
            boolean findPattern;
            boolean close = false;
            while (!close) {


                try {
                    str = sc.nextLine().trim();
                    if (str.matches("[A-zА-яІі]*") || str.equals("")) {
                        s = 0;
                    } else {
                        s = Integer.parseInt(str.trim());
                    }
                    switch (s) {
                        case 1:
                            System.out.println(ls);

                            break;
                        case 2:
                            System.out.println("нажмие 1 - добавть в начало\n" +
                                    "      2 - добавть в конец");
                            str = sc.nextLine().trim();
                            if (str.matches("[A-zА-яІі]*") || str.equals("")) {
                                s = 0;
                            } else {
                                s = Integer.parseInt(str.trim());
                            }
                            if (s == 1 || s == 2) {
                                SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                                container<String> mem = new container<>();

                                System.out.println("Введите дату 2010-2021 годов в формате dd.мм.yyyy: ");
                                pat = Pattern.compile("([0-2][0-9]|3[0-1]).(0[0-9]|1[0-2]).20(1[0-9]|2[0-1])");
                                do {
                                    mat = pat.matcher(sc.nextLine().trim());
                                    findPattern = mat.find();
                                    if (!findPattern) {
                                        System.out.println("не правильный формат даты, попробуйте еще раз. Дата 2010-2021 годов формат (dd.мм.yyyy)");
                                    }
                                } while (!findPattern);
                                String data = mat.group();
                                System.out.println("Время в формате HH:mm: ");
                                pat = Pattern.compile("([0-1][0-9]|2[0-3]):[0-5][0-9]");
                                do {
                                    mat = pat.matcher(sc.nextLine().trim());
                                    findPattern = mat.find();
                                    if (!findPattern) {
                                        System.out.println("не правильный формат времени, попробуйте еще раз. (HH:mm)");
                                    }
                                } while (!findPattern);
                                String time = mat.group();
                                Date Dat = f.parse(data + " " + time);
                                System.out.println("Место проведения: ");
                                pat = Pattern.compile("[А-я.]* ?[А-я]+ [0-9]*/?[0-9а-я]*");
                                 do {
                                    mat = pat.matcher(sc.nextLine().trim());
                                    findPattern = mat.find();
                                    if (!findPattern) {
                                        System.out.println("не правильный формат места, попробуйте еще раз.");
                                    }
                                } while (!findPattern);
                                String pl = mat.group();
                                System.out.println("Длительность формат HH:mm: ");
                                pat = Pattern.compile("[0-9][0-9]:[0-5][0-9]");
                                do {
                                    mat = pat.matcher(sc.nextLine().trim());
                                    findPattern = mat.find();
                                    if (!findPattern) {
                                        System.out.println("не правильный формат, попробуйте еще раз. (HH:mm)");
                                    }
                                } while (!findPattern);
                                StringTokenizer st = new StringTokenizer(mat.group(), ":");
                                int Dur_h = Integer.parseInt(st.nextToken().trim());
                                int Dur_m = Integer.parseInt(st.nextToken().trim());
                                System.out.println("Описание: ");
                                pat = Pattern.compile("[\\W\\w ,\\-\\d]* ?\\.?");
                                do {
                                    mat = pat.matcher(sc.nextLine().trim());
                                    findPattern = mat.find();
                                    if (!findPattern) {
                                        System.out.println("не правильный формат описания, попробуйте еще раз.");
                                    }
                                } while (!findPattern);
                                String descr = mat.group();
                                System.out.println("Количество участников: ");
                                int lim = Integer.parseInt(sc.nextLine().trim());
                                System.out.println("Формат участника: ФИО; Фамилия И. О.; (Группа)(цифры)(бквеные символы)");
                                for (int i = 0; i < lim; i++) {
                                    System.out.println("Участник №" + (i + 1) + ": ");
                                    do {
                                        str = sc.nextLine().trim();
                                        findPattern = str.matches("([А-Я][а-я]+ [А-Я]\\. [А-я]\\.|[А-Я][а-я]+ [А-Я][а-я]+ [А-Я][а-я]+|[А-Я][а-я]+ [0-9]*[А-я]*)");
                                        if (!findPattern) {
                                            System.out.println("не правильный формат записи участника, попробуйте еще раз.\n" +
                                                    "Допустимыйе форматы: ФИО; Фамилия И. О.; (Группа)(цифры)(бквеные символы)");
                                        }
                                    } while (!findPattern);
                                    mem.add(str);
                                }
                                if (s == 1) {
                                    new Thread(()-> ls.addFront(new Planner(Dat, pl, Dur_h, Dur_m, descr, mem))).start();
                                } else {
                                    new Thread(()-> ls.addBack(new Planner(Dat, pl, Dur_h, Dur_m, descr, mem))).start();

                                }
                                System.out.println("Добавлено.");
                            }

                            break;
                        case 3:

                            System.out.println("нажмие 1 - удалить с начала\n" +
                                    "      2 - удалить c конца\n" +
                                    "      3 - удалить по индексу\n");
                            str = sc.nextLine().trim();
                            if (str.matches("[A-zА-яІі]*") || str.equals("")) {
                                s = 0;
                            } else {
                                s = Integer.parseInt(str.trim());
                            }
                            if (s == 1) {
                                ls.removeFront();
                                System.out.println("Удаленно...");
                            }
                            if (s == 2) {
                                ls.removeBack();
                                System.out.println("Удаленно...");
                            }
                            if (s == 3) {
                                System.out.println("Введите индех: ");
                                str = sc.nextLine().trim();
                                if (str.matches("[A-zА-яІі]*") || str.equals("")) {
                                    s = -1;
                                } else {
                                    s = Integer.parseInt(str.trim());
                                }

                                            if (ls.remove(s)){
                                                System.out.println("Удаленно...");
                                            }else {
                                                System.out.println("Ошибка индекса");
                                            }
                            }
                            break;
                        case 4:

                            System.out.println("   1 - Поиск до определенного года\n" +
                                    "   2 - Поиск после определенного года\n" +
                                    "   3 - Поиск по минимальной длительности\n" +
                                    "   4 - Поиск индекса по данным обекта");
                            str = sc.nextLine().trim();
                            if (str.matches("[A-zА-яІі]*") || str.equals("")) {
                                s = 0;
                            } else {
                                s = Integer.parseInt(str.trim());
                            }


                            if (s == 1) {
                                System.out.println("Введите год, формат (yyyy): ");
                                str = sc.nextLine().trim();
                                if (str.matches("[0-9][0-9][0-9][0-9]")) {
                                    for (Planner ss : ls) {
                                        if (ss.searchBeforeYear(str)) {
                                            System.out.println(ss);
                                        }
                                    }
                                }else {
                                    System.out.println("Не верный формат...");
                                }


                            }
                            if (s == 2) {
                                System.out.println("Введите год, формат (yyyy): ");
                                str = sc.nextLine().trim();
                                if (str.matches("[0-9][0-9][0-9][0-9]")) {
                                    for (Planner ss : ls) {
                                        if (ss.searchAfterYear(str)) {
                                            System.out.println(ss);
                                        }
                                    }
                                }else {
                                    System.out.println("Не верный формат...");
                                }
                            }
                            if (s == 3) {
                                System.out.println("Введите миминимальную длительность, формат (HH:mm): ");
                                str = sc.nextLine().trim();
                                if (str.matches("[0-9]?[0-9]:[0-5][0-9]")) {
                                    for (Planner ss : ls) {
                                        if (ss.searchOfMinDuration(str) == 1) {
                                            System.out.println(ss);
                                        }
                                    }
                                }else {
                                    System.out.println("Не верный формат...");
                                }
                            }
                            if (s == 4) {
                                SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                                container<String> mem = new container<>();

                                System.out.println("Введите дату в формате dd.мм.yyyy: ");
                                String data = sc.nextLine().trim();
                                System.out.println("Время в формате HH:mm: ");
                                String time = sc.nextLine().trim();
                                Date Dat = f.parse(data + " " + time);
                                System.out.println("Место проведения: ");
                                String pl = sc.nextLine().trim();
                                System.out.println("Длительность (часы): ");
                                int Dur_h = Integer.parseInt(sc.nextLine().trim());
                                System.out.println("Длительность (минуты): ");
                                int Dur_m = Integer.parseInt(sc.nextLine().trim());
                                System.out.println("Описание: ");
                                String descr = sc.nextLine().trim();
                                System.out.println("Количество участников: ");
                                int lim = Integer.parseInt(sc.nextLine().trim());
                                for (int i = 0; i < lim; i++) {
                                    System.out.println("Участник №" + (i + 1) + ": ");
                                    mem.add(sc.nextLine().trim());
                                }
                                ls.indexOf(new Planner(Dat, pl, Dur_h, Dur_m, descr, mem));
                            }

                            break;
                        case 5:
                            close = true;
                            break;
                        default:
                            System.out.println("Выберите необходимое действие: \n" +
                                    "   1 - Показать список событий\n" +
                                    "   2 - Добавить\n" +
                                    "   3 - Удалить\n" +
                                    "   4 - Поиск\n" +
                                    "   5 - Завершение работы\n");
                            break;
                    }
                } catch (InputMismatchException ex) {
                    System.out.println("Необработаный выбор...");
                    break;
                }


            }
        }

    }


}

