package ua.khpi.oop.sokolov02;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        System.out.println("Число:  Сумма первых 3 чисел:  Сумма вторых 3 чисел: Результат: ");

        Random rn = new Random();                   //Число создаеться рандомно

        for (int i = 0; i < 10; i++) {
            int a = 900000 + rn.nextInt(99999);
            equal(a);
        }

        //System.out.println("SO_oKOL");
    }


    private static void equal(int a) {              //Подсчет и сравнение сумм
        System.out.print(a);
        int sum1 = 0, sum2 = 0;
        for (int i = 0; i < 6; i++) {
            if (i < 3) {
                sum2 += a % 10;
                a /= 10;
            } else {
                sum1 += a % 10;
                a /= 10;
            }

        }
        System.out.format("%23d %22d ", sum1, sum2);

        if (sum1 == sum2) {
            System.out.println("Суммы равны!!!");
        } else {
            System.out.println("Суммы не равны!!!");
        }
    }
}

