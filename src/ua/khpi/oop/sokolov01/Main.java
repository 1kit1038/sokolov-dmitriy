package ua.khpi.oop.sokolov01;


public class Main {


    private static int Pairing = 0;
    private static int unPairing = 0;
    public static void main(String []args)
    {
        int n_zal = 0x46a6;
        long n_mob = 380667223999L;
        int last_two = 0b1100011;
        int last_four = 07637;
        int mod = ((16-1)%26)+1;

        System.out.println("___________________________");
        System.out.println("Первое число: " + n_zal);
        System.out.println("Второе число: " + n_mob);
        System.out.println("Третье число: " + last_two);
        System.out.println("Четвёртое число: " + last_four);
        System.out.println("Пятое число: " + mod);


        String ss = "" + n_zal;
        char [][]char_mas = new char[5][];
        char_mas[0] = ss.toCharArray();
        ss = "" + n_mob;
        char_mas[1] = ss.toCharArray();
        ss = "" + last_two;
        char_mas[2] = ss.toCharArray();
        ss = "" + last_four;
        char_mas[3] = ss.toCharArray();
        ss = "" + mod;
        char_mas[4] = ss.toCharArray();

        pairing(char_mas);
        System.out.println("___________________________");
        System.out.println("Количество чётных: " + Pairing);
        System.out.println("Количество не чётных: " + unPairing);
        System.out.println("___________________________");
        long[] mas = {n_zal,n_mob,last_two,last_four,mod};
        to_bin(mas);

    }

    private static void pairing(char[][] mas) {     //Подсчет четных и не четных

        {
            for (char[] ss : mas) {
                for (char s : ss) {
                    if (s % 2 == 1) {
                        unPairing++;
                    } else {
                        Pairing++;
                    }
                }
            }
        }
    }
    private static void to_bin(long []mas){         //Перевод числа в двоичный код
        long bit;
        int cl_one;
        String temp;
        for (long a : mas){
             temp = "";
             cl_one = 0;
            System.out.println("Число: " + a);
            while (a != 0) {
                bit = a % 2;
                temp = bit + "." + temp;
                if (bit == 1) {
                    cl_one++;
                }
                a /= 2;
            }
            System.out.println("В двоичной системе: " + temp);
            System.out.println("Количество единиц: " + cl_one);

        }
    }
}
