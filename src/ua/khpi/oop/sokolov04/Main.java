package ua.khpi.oop.sokolov04;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

public class Main {

    public static void main(String[] args){

        String fileP = "resources//sokolov03_text.txt";

        Vector<String> mas1 = new Vector<>();
        Vector<String> mas2 = new Vector<>();
        Vector<String> mas3 = new Vector<>();
        Scanner sc = new Scanner(System.in);



        System.out.println("Выберите удобный способ ввода текста для анализа: \n" +
                "   1 - Чтение из файла (файл дольшен находиться в resources)\n" +
                "   2 - Ручной воод\n" +
                "   3 - Вывод считаного текста\n" +
                "   4 - Вывод результата\n" +
                "   5 - Завершение работы\n");


        String str = "";
        Vector<String> ss;
        boolean close = false;
        while (!close) {


            try {
                switch (sc.nextInt()) {
                    case 1:
                        System.out.println("Введите имя файла");
                        fileP = "resources//" + sc.next();
                        try {
                            str = readFile(fileP);
                            ss = convertToVec(str);
                            sortToMass(ss, mas1, mas2, mas3);
                            break;
                        } catch (NullPointerException ex) {
                            break;
                        }

                    case 2:

                        System.out.println("Введите текст");
                        str = sc.next() + sc.nextLine();
                        ss = convertToVec(str);
                        sortToMass(ss, mas1, mas2, mas3);
                        break;
                    case 3:

                        if (str == "") {
                            System.out.println("Введите текст!");
                            break;
                        }
                        System.out.println(str);
                        break;
                    case 4:
                        System.out.println("mas1: " + mas1);
                        System.out.println("mas2: " + mas2);
                        System.out.println("mas3: " + mas3);
                        break;
                    case 5:
                        close = true;
                        break;
                    default:
                        System.out.println("Необработаный вызов\n" +
                                "   1 - Чтение из файла (файл дольшен находиться в resources)\n" +
                                "   2 - Ручной воод\n" +
                                "   3 - Вывод считаного текста\n" +
                                "   4 - Вывод результата\n" +
                                "   5 - Завершение работы\n");
                        break;
                }
            }catch (InputMismatchException ex){
                System.out.println("Необработаный выбор...");
                break;
            }


        }

    }

    private static String readFile(String FileP){

        try {
            BufferedReader F = new BufferedReader(new FileReader(FileP));

            StringBuilder read = new StringBuilder();
            String ln = F.readLine();
            while (ln != null) {
                read.append(ln);
                ln = F.readLine();
                read.append("\n");
            }
            return read.toString();

        }catch (IOException ex){
            System.out.println(ex.getMessage());
            return null;
        }

    }

    private static Vector<String> convertToVec (String str){

        Vector<String> ss = new Vector<>();
        int f = 0;
        str = str.replace("\n","");
        str += '\n';
        for (int i = 0; i < str.length(); i++){
            if (str.toCharArray()[i] == ' ' || str.toCharArray()[i] == ',' || str.toCharArray()[i] == '.' || str.toCharArray()[i] == '-'|| str.toCharArray()[i] == '\n') {
                ss.add(str.substring(f, i));
                while (str.toCharArray()[i] == ' ' || str.toCharArray()[i] == ',' || str.toCharArray()[i] == '.' || str.toCharArray()[i] == '-'){
                    i++;
                }
                f = i;
            }
        }
        return ss;
    }
    private static void sortToMass(Vector<String> in, Vector<String> out1, Vector<String> out2, Vector<String> out3){

        String gl = "АаОоИиЕеЁёЭэыУуЮюЯЯ";
        String sg = "БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфХхЦцЧчШшЩщ";
        skip:
        for (String b : in) {

            for (int j = 0; j < gl.length(); j++) {
                if (gl.toCharArray()[j] == b.toCharArray()[0]) {
                    out1.add(b);
                    continue skip;

                }
            }
            for (int j = 0; j < sg.length(); j++) {
                if (sg.toCharArray()[j] == b.toCharArray()[0]) {
                    out2.add(b);
                    continue skip;

                }

            }
            out3.add(b);

        }

    }


}
