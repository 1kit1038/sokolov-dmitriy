package ua.khpi.oop.sokolov14;


import java.text.ParseException;
import java.util.Random;

public class Threads {

    public static void start() throws InterruptedException {

        Random random = new Random();
        int time = random.nextInt(5);
        List<Planner> ls = new List<>();

        Runnable rn1 = ()->{
            try {
                Thread.sleep(2);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read1 = new Planner("12.06.2017 14:00", "ул. Академіка Павлова 271", 36, 0, "Марафон с++", "Студетны 118a, Студетны 118b");
                    read1.searchOfMinDuration("35:00");
                    System.out.println(read1);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Первй поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Runnable rn2 = ()->{
            try {
                Thread.sleep(3);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read2 = new Planner("12.04.2020 13:30", "ул.Жилянська 75", 72, 0, "Практика в EPAM", "Студетны 118a, Студетны 118b, Студетны 118є, Студетны 201a");
                    System.out.println(read2);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Второй поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Runnable rn3 = ()-> {
            try {
                Thread.sleep(2);
                if (!Thread.currentThread().isInterrupted()) {

                    Planner read3 = new Planner("15.06.2020 12:30; ул.Пушкинская 15/2; 12; 40; Собрание; Студетны 118е");
                    System.out.println(read3);

                } else {
                    throw new InterruptedException();
                }
            }catch (InterruptedException e){
                System.out.println("Третий поток остановлен");
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        };

        Thread thr1 = new Thread(rn1);
        Thread thr2 = new Thread(rn2);
        Thread thr3 = new Thread(rn3);

        thr1.start();
        thr2.start();
        thr3.start();
        System.out.println("Все потоки запущенны");
        Thread.sleep(time);
        thr1.interrupt();
        thr2.interrupt();
        thr3.interrupt();



    }


    static int rez = 0;
    public static void start_2(List<Planner> src) throws InterruptedException {

        class Thr extends Thread{

            public final List<Planner> ls;

            public Thr(List<Planner> src){
                ls = src;
            }

            @Override
            public void run() {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (Planner l : ls){
                    if (l.searchOfMinDuration("70:00") == 1){
                        System.out.println("\nПоток № " + ++rez + " закончил");
                    }
                }

            }
        }

        long time_start = System.currentTimeMillis();
        Thr thr1 = new Thr(src);
        Thr thr2 = new Thr(src);
        Thr thr3 = new Thr(src);

        System.out.println("Начало паралельной обработки:");
        thr1.start();
        thr2.start();
        thr3.start();
        thr1.join();
        thr2.join();
        thr3.join();
        long time_stop = System.currentTimeMillis() - time_start;

        System.out.println("\nВремя выполнения паралельной обработки: " + time_stop);

        System.out.println("\nНачало последовательной обработки:");
        rez = 0;
        time_start = System.currentTimeMillis();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Planner l : src){
            if (l.searchOfMinDuration("70:00") == 1){
                System.out.println("\nОперация № " + ++rez + " закончилась");
            }
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Planner l : src){
            if (l.searchOfMinDuration("70:00") == 1){
                System.out.println("\nОперация № " + ++rez + " закончилась");
            }
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Planner l : src){
            if (l.searchOfMinDuration("70:00") == 1){
                System.out.println("\nОперация № " + ++rez + " закончилась");
            }
        }
        time_stop = System.currentTimeMillis() - time_start;
        System.out.println(time_stop);
        System.out.println("\nВремя выполнения послеовательной обработки: " + time_stop);



    }

}
