package ua.khpi.oop.sokolov14;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;


public class Planner implements Serializable {

    private Date date;
    private int duration_hour;
    private int duration_minute;


    private String place;
    private String description;
    private container<String> members;


    public boolean searchBeforeYear(String year) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return date.before(sdf.parse(year));
    }

    public boolean searchAfterYear(String year) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return date.after(sdf.parse(year));
    }

    public int searchOfMinDuration(String duration){


            if (!duration.matches("[0-9]?[0-9]:[0-5][0-9]")) {
                System.out.println("Не правильный формат, попробуйте еще раз. (HH:mm)");
                return -1;
            }

        StringTokenizer st = new StringTokenizer(duration, ":");
        int Dur_h = Integer.parseInt(st.nextToken().trim());
        int Dur_m = Integer.parseInt(st.nextToken().trim());

        if((Dur_h * 60 + (Dur_m)) - (duration_hour * 60 + (duration_minute)) <= 0 ){
            return 1;
        }

        return 0;
    }


    public Planner(Date date, String place, int duration_hour, int duration_minute, String description, container<String> members) {
        this.date = date;
        this.place = place;
        this.duration_hour = duration_hour;
        this.duration_minute = duration_minute;
        this.description = description;
        this.members = members;
    }

    public Planner(String date, String place, int duration_hour, int duration_minute, String description, String members) throws ParseException {
        this.members = new container<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        StringTokenizer st = new StringTokenizer(members, ",");
        this.date = sdf.parse(date);
        this.place = place;
        this.duration_hour = duration_hour;
        this.duration_minute = duration_minute;
        this.description = description;
        for (int i = 0; i <= st.countTokens(); i++) {
            this.members.add(st.nextToken());
        }
    }


    public Planner(String info) throws ParseException {
        this.members = new container<>();
        StringTokenizer st = new StringTokenizer(info, ";");
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        date = sdf.parse(st.nextToken().trim());
        place = st.nextToken().trim();
        duration_hour = Integer.parseInt(st.nextToken().trim());
        duration_minute = Integer.parseInt(st.nextToken().trim());
        description = st.nextToken().trim();
        for (int i = 0; i <= st.countTokens(); i++) {
            members.add(st.nextToken().trim());
        }
    }

    @Override
    public String toString() {

        SimpleDateFormat sd = new SimpleDateFormat("dd:MM:yyyy в HH:mm");
        return "\nДата: " + sd.format(date) +
                "\nАдресс: " + place +
                "\nДлительность: " + duration_hour + ":" + duration_minute +
                "\nОписание:" + description +
                "\nУчастники: " + members.toString() + "\n";

    }

    Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration_hour() {
        return duration_hour;
    }

    public void setDuration_hour(int time) {
        this.duration_hour = time;
    }

    public int getDuration_minute() {
        return duration_minute;
    }

    public void setDuration_minute(int duration_minute) {
        this.duration_minute = duration_minute;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public container<String> getMembers() {
        return members;
    }

    public void setMembers(container<String> members) {
        this.members = members;
    }
}
