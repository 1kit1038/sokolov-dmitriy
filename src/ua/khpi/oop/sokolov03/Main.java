package ua.khpi.oop.sokolov03;


import java.io.*;
import java.text.ParseException;
import java.util.Vector;

public class Main {

    public static void main(String[] args) throws ParseException, IOException, ClassNotFoundException {

        String fileP = "resources//sokolov03_text.txt";
        String str = readFile(fileP);
        System.out.println(str);

        Vector<String> ss = convertToVec(str);
        System.out.println(ss + "\n");

        Vector<String> mas1 = new Vector<>();
        Vector<String> mas2 = new Vector<>();
        Vector<String> mas3 = new Vector<>();

        sortToMass(ss, mas1, mas2, mas3);
        System.out.println("mas1: " + mas1);
        System.out.println("mas2: " + mas2);
        System.out.println("mas3: " + mas3);

    }

    protected static String readFile(String FileP){

        try {
            BufferedReader F = new BufferedReader(new FileReader(FileP));

            StringBuilder read = new StringBuilder();
            String ln = F.readLine();
            while (ln != null) {
                read.append(ln);
                ln = F.readLine();
                read.append("\n");
            }
            return read.toString();

        }catch (IOException ex){
            System.out.println(ex.getMessage());
            return null;
        }

    }

    protected static Vector<String> convertToVec(String str){

        Vector<String> ss = new Vector<>();
        int f = 0;
        str = str.replace("\n","");
        str += '\n';
        for (int i = 0; i < str.length(); i++){
            if (str.toCharArray()[i] == ' ' || str.toCharArray()[i] == ',' || str.toCharArray()[i] == '.' || str.toCharArray()[i] == '-') {
                ss.add(str.substring(f, i));
                while (str.toCharArray()[i] == ' ' || str.toCharArray()[i] == ',' || str.toCharArray()[i] == '.' || str.toCharArray()[i] == '-'){
                    i++;
                }
                f = i;
            }
        }
        return ss;
    }
    protected static void sortToMass(Vector<String> in, Vector<String> out1, Vector<String> out2, Vector<String> out3){

        String gl = "АаОоИиЕеЁёЭэыУуЮюЯЯ";
        String sg = "БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфХхЦцЧчШшЩщ";
        skip:
        for (String b : in) {

            for (int j = 0; j < gl.length(); j++) {
                if (gl.toCharArray()[j] == b.toCharArray()[0]) {
                    out1.add(b);
                    continue skip;

                }
            }
            for (int j = 0; j < sg.length(); j++) {
                if (sg.toCharArray()[j] == b.toCharArray()[0]) {
                    out2.add(b);
                    continue skip;

                }

            }
            out3.add(b);

        }

    }

}