package ua.khpi.oop.sokolov11;

import java.util.ListIterator;


class item<E> {
    E data;
    item<E> prev;
    item<E> next;


    item(item<E> prev, item<E> next, E data){
        this.data = data;
        this.prev = prev;
        this.next = next;
        if(prev != null){
            this.prev.next = this;
        }
        if(next != null){
            this.next.prev = this;
        }
    }

}

public class List<E> implements Iterable<E> {

    int size = 0;
    private item<E> firstItem, lastItem;

    public List() {
        firstItem = null;
        lastItem = null;
    }

    void addBack(E data) {

        if (firstItem == null) {
            item<E> newItem = new item<>(null, null, data);
            firstItem = newItem;
            lastItem = newItem;
        } else {
            lastItem = new item<>(lastItem, null, data);
            //lastItem.prev.next = lastItem;
        }
        size++;
    }

    void addFront(E data) {

        if (firstItem == null) {
            item<E> newItem = new item<>(null, null, data);
            firstItem = newItem;
            lastItem = newItem;
        } else {
            firstItem = new item<>(null, firstItem, data);
            //firstItem.next.prev=firstItem;
        }
        size++;
    }

    public void removeBack(){

        if (!lastItem.equals(firstItem)) {
            lastItem = lastItem.prev;
            lastItem.next.prev = null;
            lastItem.next = null;
        }else {
            firstItem = null;
            lastItem = null;
        }
        size--;
    }

    public void remove(int index){
        if (index < size) {
        item<E> temp = firstItem;

            if (!firstItem.equals(lastItem)) {
                while (index != 0) {
                    temp = temp.next;
                    index--;
                }
                if (temp.equals(firstItem)){
                    removeFront();
                    size--;
                    return;
                }
                if (temp.equals(lastItem)){
                    removeBack();
                    size--;
                    return;
                }
                temp.next.prev = temp.prev;
                temp.prev.next = temp.next;

            } else {
                firstItem = null;
                lastItem = null;
            }
            size--;
        }
    }

    public void removeFront(){

        if (!firstItem.equals(lastItem)) {
            firstItem = firstItem.next;
            firstItem.prev.next = null;
            firstItem.prev = null;
        }else {
            firstItem = null;
            lastItem = null;
        }
        size--;
    }


    public int indexOf(E search){
        int i = 0;
        item<E> temp = firstItem;
        while (temp != null) {
            if (temp.data.equals(search)){
                return i;
            }
            temp = temp.next;
            i++;
        }

      return -1;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (firstItem == null && lastItem == null){
            sb.append("Список пуст...");
            return sb.toString();
        }
        item<E> temp = firstItem;
        sb.append(temp.data).append(" ");
        while (temp.next != null){
            temp = temp.next;
            sb.append(temp.data);
            if (temp.next != null) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    @Override
    public ListIterator<E> iterator() {

        return new ListIterator<>() {
            item<E> temp = firstItem;
            item<E> out;
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public E next() {
                out = temp;
                temp = temp.next;
                index++;
                return out.data;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public E previous() {
                return null;
            }

            @Override
            public int nextIndex() {
                return 0;
            }

            @Override
            public int previousIndex() {
                return 0;
            }

            @Override
            public void remove() {

            }

            @Override
            public void set(E e) {

            }

            @Override
            public void add(E e) {

            }
        };
    }


}
