package ua.khpi.oop.sokolov06;

import java.util.Vector;

public class Main extends ua.khpi.oop.sokolov03.Main {


    public static void main(String[] args) {

        String fileP = "resources//sokolov03_text.txt";
        String str = readFile(fileP);
        System.out.println(str);

        Vector<String> ss = convertToVec(str);
        System.out.println(ss + "\n");

        //Vector<String> mas1 = new Vector<>();
        //Vector<String> mas2 = new Vector<>();
        //Vector<String> mas3 = new Vector<>();
        container<String> mas1 = new container<String>();
        container<String> mas2 = new container<String>();
        container<String> mas3 = new container<String>();

        sortToMass(ss, mas1, mas2, mas3);
        System.out.println("mas1: " + mas1);
        System.out.println("mas2: " + mas2);
        System.out.println("mas3: " + mas3);

//        System.out.println(mas2.size());
//        mas2.remove("говоря");
//        mas2.remove("промежуток");
//        mas2.remove("полученными");
//        mas2.remove("в");
//        mas2.remove("случае");
//        mas2.remove("потери");
//        System.out.println("mas2: " + mas2);
//        System.out.println(mas2.size());

//        System.out.println("rez_1= " + mas1.contains("интерполироваться"));
//        System.out.println("rez_2= " + mas1.contains("ыы"));
//        container<String> mas4 = new container<String>();
//        mas4.add("интервал");
//        mas4.add("около");
//        mas4.add("интерполироваться");
//        System.out.println("rez_3= " + mas1.containsAll(mas4));


    }


    private static void sortToMass(Vector<String> in, container<String> out1, container<String> out2, container<String> out3) {

        String gl = "АаОоИиЕеЁёЭэыУуЮюЯЯ";
        String sg = "БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфХхЦцЧчШшЩщ";
        skip:
        for (String b : in) {

            for (int j = 0; j < gl.length(); j++) {
                if (gl.toCharArray()[j] == b.toCharArray()[0]) {
                    out1.add(b);
                    continue skip;

                }
            }
            for (int j = 0; j < sg.length(); j++) {
                if (sg.toCharArray()[j] == b.toCharArray()[0]) {
                    out2.add(b);
                    continue skip;

                }

            }
            out3.add(b);

        }

    }

}
