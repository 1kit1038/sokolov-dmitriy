package ua.khpi.oop.sokolov10;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class Main {


    public static void main(String[] args) throws ParseException, IOException, ClassNotFoundException {


        List<Planner> ls = new List<>();

        if (args.length == 1 && args[0].trim().equals("-auto")) {

            Planner read1 = new Planner("12.06.2019 14:00", "ул.Пушкинская 15/2", 75, "Собрание", "Студетны 118a, Студетны 118b");

            Planner read2 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 55, "Консультация КСХ", "Студетны 118a, Студетны 201a");

            Planner read3 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 55, "Экзамен КСХ", "Студетны 118a, Студетны 201a");


            System.out.println("Создание листа");
            System.out.println(ls);
            System.out.println("\n\nЗаполнение");
            ls.addBack(read1);
            ls.addBack(read2);
            ls.addBack(read3);
            System.out.println("Три элемента записанно");
            System.out.println("\n Удаление с начала и с конца");
            ls.removeBack();
            ls.removeFront();
            System.out.println(ls);
            System.out.println("\n\nЗаполнение в другом порядке");
            ls.addBack(read1);
            ls.addFront(read3);
            System.out.println(ls);

        }else {

            Planner read1 = new Planner("12.06.2019 14:00", "ул.Пушкинская 15/2", 75, "Собрание", "Студетны 118a, Студетны 118b");

            Planner read2 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 55, "Консультация КСХ", "Студетны 118a, Студетны 201a");

            Planner read3 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 55, "Экзамен КСХ", "Студетны 118a, Студетны 201a");
            ls.addBack(read1);
            ls.addBack(read2);
            ls.addBack(read3);
            System.out.println("Выберите удобный способ ввода текста для анализа: \n" +
                    "   1 - Показать список событий\n" +
                    "   2 - Добавить\n" +
                    "   3 - Удалить\n" +
                    "   4 - Поиск индекса\n" +
                    "   5 - Завершение работы\n");
            Scanner sc = new Scanner(System.in);

            String str;
            int s;
            boolean close = false;
            while (!close) {


                try {
                    str = sc.nextLine().trim();
                    if (str.equals("")) {
                        s = 0;
                    } else {
                        s = Integer.parseInt(str.trim());
                    }
                    switch (s) {
                        case 1:
                                System.out.println(ls);

                            break;
                        case 2:
                            System.out.println("нажмие 1 - добавть в начало\n" +
                                    "      2 - добавть в конец");
                            str = sc.nextLine().trim();
                            if (str.equals("")) {
                                s = 0;
                            } else {
                                s = Integer.parseInt(str.trim());
                            }
                            if (s == 1 || s == 2) {
                                SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                                container<String> mem = new container<>();

                                System.out.println("Введите дату в формате dd.мм.yyyy: ");
                                String data = sc.nextLine().trim();
                                System.out.println("Время в формате HH:mm: ");
                                String time = sc.nextLine().trim();
                                Date Dat = f.parse(data + " " + time);
                                System.out.println("Место проведения: ");
                                String pl = sc.nextLine().trim();
                                System.out.println("Длительность: ");
                                int Dur = Integer.parseInt(sc.nextLine().trim());
                                System.out.println("Описание: ");
                                String descr = sc.nextLine().trim();
                                System.out.println("Количество участников: ");
                                int lim = Integer.parseInt(sc.nextLine().trim());
                                for (int i = 0; i < lim; i++) {
                                    System.out.println("Участник №" + (i + 1) + ": ");
                                    mem.add(sc.nextLine().trim());
                                }
                                if (s == 1) {
                                    ls.addFront(new Planner(Dat, pl, Dur, descr, mem));
                                } else {
                                    ls.addBack(new Planner(Dat, pl, Dur, descr, mem));

                                }
                                System.out.println("Добавлено.");
                            }

                            break;
                        case 3:

                            System.out.println("нажмие 1 - удалить с начала\n" +
                                    "      2 - удалить c конца\n"+
                                    "      3 - удалить по индексу\n");
                            str = sc.nextLine().trim();
                            if (str.equals("")) {
                                s = 0;
                            } else {
                                s = Integer.parseInt(str.trim());
                            }
                            if (s == 1) {
                                ls.removeFront();
                                System.out.println("Удаленно...");
                            }
                            if (s == 2) {
                                ls.removeBack();
                                System.out.println("Удаленно...");
                            }
                            if (s == 3){
                                System.out.println("Введите индех: ");
                                str = sc.nextLine().trim();
                                if (str.equals("")) {
                                    s = 0;
                                } else {
                                    s = Integer.parseInt(str.trim());
                                }
                                ls.remove(s);
                                System.out.println("Удаленно...");
                            }
                            break;
                        case 4:

                            SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                            container<String> mem = new container<>();

                            System.out.println("Введите дату в формате dd.мм.yyyy: ");
                            String data = sc.nextLine().trim();
                            System.out.println("Время в формате HH:mm: ");
                            String time = sc.nextLine().trim();
                            Date Dat = f.parse(data + " " + time);
                            System.out.println("Место проведения: ");
                            String pl = sc.nextLine().trim();
                            System.out.println("Длительность: ");
                            int Dur = Integer.parseInt(sc.nextLine().trim());
                            System.out.println("Описание: ");
                            String descr = sc.nextLine().trim();
                            System.out.println("Количество участников: ");
                            int lim = Integer.parseInt(sc.nextLine().trim());
                            for (int i = 0; i < lim; i++) {
                                System.out.println("Участник №" + (i + 1) + ": ");
                                mem.add(sc.nextLine().trim());
                            }
                            ls.indexOf(new Planner(Dat, pl, Dur, descr, mem));
                            break;
                        case 5:
                            close = true;
                            break;
                        default:
                            System.out.println("Выберите удобный способ ввода текста для анализа: \n" +
                                    "   1 - Показать список событий\n" +
                                    "   2 - Добавить\n" +
                                    "   3 - Удалить\n" +
                                    "   4 - Поиск инекса\n" +
                                    "   5 - Завершение работы\n");
                            break;
                    }
                } catch (InputMismatchException ex) {
                    System.out.println("Необработаный выбор...");
                    break;
                }


            }
        }

    }


}

