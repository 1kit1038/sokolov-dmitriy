package ua.khpi.oop.sokolov09;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {


    public static void main(String[] args) throws ParseException, IOException, ClassNotFoundException {


        SimpleDateFormat form = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        container<String> con = new container<>();

        String inf = "15.02.2019 15:30; ул.Пушкинская 15/2; 75; Собрание; Студетны 118е";
        StringTokenizer st = new StringTokenizer(inf,";");

        Date Data = form.parse(st.nextToken());
        String place = st.nextToken();
        String dur = st.nextToken();
        int Duration = Integer.parseInt(dur.substring(1));
        String description = st.nextToken();
        for (int i = 0; i < st.countTokens(); i++){
            con.add(st.nextToken());
        }
        Planner write = new Planner(Data, place, Duration, description, con);

        Planner write1 = new Planner("15.06.2020 12:30; ул.Пушкинская 15/2; 40; Собрание; Студетны 118е");


        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("resources//sokolov09_Serializa.obj"));
        oos.writeObject(write);
        oos.writeObject(write1);
        System.out.println("Первое событие");
        System.out.println(write);
        System.out.println("Второе событие");
        System.out.println(write1);
        System.out.println("Первое и второе событие серелизовны");

        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("resources//sokolov09_Serializa.obj"));
        Planner read = (Planner) ois.readObject();
        Planner read1 = (Planner) ois.readObject();
        System.out.println("Первое и второе событие десерелизовны");

        ois.close();


        Planner read3 = new Planner("12.04.2020 13:30", "ул.Пушкинская 15/2", 55, "Собрание", "Студетны 118a");

        System.out.println("создано третье событие и добавленно в спиок");

        List<Planner> ls = new List<>();
        ls.addBack(read);
        ls.addBack(read1);
        ls.addBack(read3);
        ls.removeBack();
        ls.removeFront();
        ls.addBack(read);
        ls.addFront(read1);

        System.out.println(ls);


    }


}

