package ua.khpi.oop.sokolov09;


import java.io.Serializable;
import java.util.Iterator;

public class container<E> implements Iterable<String>, Serializable {


    private String[] mas;
    private int index = 0;


    container() {
        mas = new String[0];
    }

    @Override
    public String toString() {

        StringBuilder ss = new StringBuilder();
        ss.append("[ ");
        for (String ma : mas) {
            if (ma != null) {
                ss.append(ma).append(" ");
            }
        }
        ss.append("]");
        return ss.toString();
    }

    public void add(String string) {
        if (mas.length == 0 || index == mas.length) {
            String[] t = mas;
            mas = new String[t.length + 5];
            mas[index] = string;
            index++;
            System.arraycopy(t, 0, mas, 0, t.length);
        } else {
            mas[index] = string;
            index++;
        }
    }

    public void clear() {
        mas = new String[0];
    }

    public boolean remove(String string) {
        for (int i = 0; i < index; i++) {
            if (mas[i].equals(string)) {

                String[] temp = mas;
                if (index > temp.length - 5) {
                    mas = new String[temp.length];
                } else {
                    mas = new String[temp.length - 5];
                }
                System.arraycopy(temp, 0, mas, 0, i);
                System.arraycopy(temp, i + 1, mas, i, index - i - 1);
                index--;
                return true;
            }

        }
        return false;

    }

    public Object[] toArray() {
        return mas;
    }

    public int size() {
        return index;
    }

    public boolean contains(String string) {
        for(int i = 0; i < index; i++) {
            if (mas[i].contains(string)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(container<String> container) {
        boolean rez = true;
        String ss = this.toString();

        for (String cont : container) {
            rez &= ss.contains(cont);
        }

        return rez;
    }

    private int iterl = 0;

    @Override
    public Iterator<String> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return iterl < index;
            }

            @Override
            public String next() {
                return mas[iterl++];
            }

            @Override
            public void remove() {
                iterl = 0;
            }
        };
    }
}

