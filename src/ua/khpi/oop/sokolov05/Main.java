package ua.khpi.oop.sokolov05;

//import ua.khpi.oop.sokolov03.Main.*;

import java.util.Vector;

public class Main extends ua.khpi.oop.sokolov03.Main {


    public static void main(String[] args) {

        String fileP = "resources//sokolov03_text.txt";
        String str = readFile(fileP);
        System.out.println(str);

        Vector<String> ss = convertToVec(str);
        System.out.println(ss + "\n");

        //Vector<String> mas1 = new Vector<>();
        //Vector<String> mas2 = new Vector<>();
        //Vector<String> mas3 = new Vector<>();
        container<String> mas1 = new container<String>();
        container<String> mas2 = new container<String>();
        container<String> mas3 = new container<String>();

        sortToMass(ss, mas1, mas2, mas3);
        System.out.println("mas1: " + mas1);
        System.out.println("mas2: " + mas2);
        System.out.println("mas3: " + mas3);
    }



    private static void sortToMass(Vector<String> in, container<String> out1, container<String> out2, container<String> out3) {

        String gl = "АаОоИиЕеЁёЭэыУуЮюЯЯ";
        String sg = "БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфХхЦцЧчШшЩщ";
        skip:
        for (String b : in) {

            for (int j = 0; j < gl.length(); j++) {
                if (gl.toCharArray()[j] == b.toCharArray()[0]) {
                    out1.add(b);
                    continue skip;

                }
            }
            for (int j = 0; j < sg.length(); j++) {
                if (sg.toCharArray()[j] == b.toCharArray()[0]) {
                    out2.add(b);
                    continue skip;

                }

            }
            out3.add(b);

        }

    }

}
